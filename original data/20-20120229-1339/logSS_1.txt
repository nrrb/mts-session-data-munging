AMODE|Move|02:14-7:28 PM
SCELL|Active;17|02:22-7:28 PM
MVSLF|C2|02:31-7:28 PM
CICON|Ally|02:40-7:28 PM
CICON|Ally|02:41-7:28 PM
CICON|Ally|02:47-7:28 PM
CICON|Self|02:47-7:28 PM
CICON|Ally|02:50-7:28 PM
CICON|Ally|02:51-7:28 PM
CICON|Self|02:55-7:28 PM
CICON|Ally|03:01-7:28 PM
SCELL|Active;17|03:01-7:28 PM
CICON|Self|03:03-7:28 PM
CICON|Self|03:30-7:29 PM
AMODE|Identify|03:49-7:29 PM
IDENT|A Tire Fire Target has been identified in map cell C2 at position 93, 265.|04:35-7:30 PM
IDENT|A Tire Fire Target has been identified in map cell C2 at position 430, 272.|04:52-7:30 PM
IDENT|A Van Threat has been identified in map cell C2 at position 216, 131.|05:22-7:31 PM
CICON|Ally|06:36-7:32 PM
CICON|Self|06:37-7:32 PM
SCELL|Active;17|06:38-7:32 PM
CICON|Target|06:39-7:32 PM
SCELL|Active;17|06:42-7:32 PM
SCELL|Active;27|06:44-7:32 PM
SCELL|Active;17|06:45-7:32 PM
AMODE|Engage|06:49-7:32 PM
ENGAG|System.Windows.Forms.Label, Text: C2;Lutet~PETN|06:59-7:32 PM
NEUTR|Threat~Van~C2~216,131~1|07:06-7:32 PM
SCELL|Active;17|07:09-7:32 PM
UPMAP|NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN|07:11-7:33 PM
SCELL|Active;17|07:14-7:33 PM
UPMAP|NNNNNNNNNNNNNNNNNGNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN|07:15-7:33 PM
SCELL|Active;17|07:19-7:33 PM
AMODE|Move|08:06-7:33 PM
SCELL|Active;37|08:08-7:33 PM
MVSLF|C4|08:12-7:34 PM
AMODE|Identify|08:28-7:34 PM
SCELL|Active;37|09:13-7:35 PM
AMODE|Move|10:18-7:36 PM
SCELL|Active;35|10:21-7:36 PM
MVSLF|E4|10:25-7:36 PM
AMODE|Identify|10:36-7:36 PM
IDENT|A Tire Fire Target has been identified in map cell E4 at position 239, 431.|10:51-7:36 PM
IDENT|A Sedan Threat has been identified in map cell E4 at position 202, 252.|11:07-7:36 PM
IDENT|A Tire Fire Target has been identified in map cell E4 at position 65, 199.|11:24-7:37 PM
IDENT|A Radio Tower Target has been identified in map cell E4 at position 279, 123.|12:43-7:38 PM
AMODE|Engage|13:21-7:39 PM
ENGAG|System.Windows.Forms.Label, Text: E4;Iridi~C4|13:33-7:39 PM
NEUTR|Threat~Sedan~E4~202,252~1|13:38-7:39 PM
SCELL|Active;35|13:41-7:39 PM
SCELL|Active;35|13:42-7:39 PM
UPMAP|NNNNNNNNNNNNNNNNNGNNNNNNNNNNNNNNNNNGNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN|13:45-7:39 PM
SCELL|Active;5|13:49-7:39 PM
AMODE|Move|13:53-7:39 PM
SCELL|Active;5|13:54-7:39 PM
SCELL|Active;5|13:55-7:39 PM
MVSLF|E1|13:57-7:39 PM
AMODE|Identify|14:24-7:40 PM
IDENT|A Tire Fire Target has been identified in map cell E1 at position 232, 234.|14:38-7:40 PM
IDENT|A Radio Tower Target has been identified in map cell E1 at position 373, 369.|14:50-7:40 PM
IDENT|A Van Threat has been identified in map cell E1 at position 477, 271.|15:34-7:41 PM
