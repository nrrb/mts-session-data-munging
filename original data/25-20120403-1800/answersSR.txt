START|4/3/2012 4:16:22 PM
MEASURE|Demographics
Gender|0
Race|6
Age|19
PCVidPlay|0
VidPlay|4
Iming|5
TeamConf|3
SkypeConf|4
VidLearnConf|4
Phase2|1
MEASURE|Competitiveness
Comp_1|2
Comp_2|2
Comp_3|2
Comp_4|2
Comp_5|2
Comp_6|2
Comp_7|2
Comp_8|1
Comp_9|1
Comp_10|2
Comp_11|2
Comp_12|3
Comp_13|3
Comp_14|2
Comp_15|1
Comp_16|0
Comp_17|2
Comp_18|2
Comp_19|1
Comp_20|3
MEASURE|Self- and Other-Orientation
Prosocial_1|3
Prosocial_2|3
Prosocial_3|3
Prosocial_4|4
Prosocial_5|3
Prosocial_6|3
MEASURE|Information Sharing Network
IS_Net1|1:5
IS_Net1|2:5
IS_Net1|3:4
MEASURE|Transactive Memory Systems
TMS1_Net1|1:5
TMS1_Net1|2:5
TMS1_Net1|3:5
MEASURE|Transactive Memory Systems
TMS_1|4
TMS_2|3
TMS_3|3
TMS_4|4
TMS_5|4
TMS_6|3
TMS_7|3
TMS_8|3
TMS_9|1
TMS_10|0
MEASURE|Trust1_Net
Trust1_1|t:3
Trust1_1|T:3
Trust1_2|t:3
Trust1_2|T:3
Trust1_3|t:4
Trust1_3|T:3
Trust1_4|t:3
Trust1_4|T:3
Trust1_5|t:3
Trust1_5|T:4
Trust1_6|t:3
Trust1_6|T:4
MEASURE|Trust2
Trust2_Net|1:4
Trust2_Net|2:5
Trust2_Net|3:3
MEASURE|Action Process
P-Act1|0
P-Act2|4
P-Act3|2
P-Act4|1
P-Act5|2
P-Act6|1
P-Act7|2
P-Act8|1
P-Act9|2
MEASURE|Action Process
P-Act_Net|1:3
P-Act_Net|2:3
P-Act_Net|3:1
MEASURE|Transactive Memory Systems
TMS1_Net|1:3
TMS1_Net|2:5
TMS1_Net|3:3
TMS2_Net|1:4
TMS2_Net|2:5
TMS2_Net|3:2
TMS3_Net|1:4
TMS3_Net|2:5
TMS3_Net|3:1
TMS4_Net|1:3
TMS4_Net|2:3
TMS4_Net|3:1
MEASURE|Infomration Sharing
IS1|2
IS2|4
IS3|1
MEASURE|Information Sharing Network
IS_Net1|1:3
IS_Net1|2:5
IS_Net1|3:1
IS_Net2|1:4
IS_Net2|2:5
IS_Net2|3:1
MEASURE|Social Identity
SocId1|t:3
SocId1|T:2
SocId2|t:1
SocId2|T:2
SocId3|t:1
SocId3|T:1
SocId4|t:2
SocId4|T:3
SocId5|t:3
SocId5|T:1
SocId6|t:4
SocId6|T:1
SocId7|t:3
SocId7|T:2
SocId8|t:3
SocId8|T:4
SocId9|t:1
SocId9|T:1
SocId10|t:3
SocId10|T:4
MEASURE|Prosoical Behavior
Pro_Beh_1|t:0
Pro_Beh_1|T:0
Pro_Beh_2|t:3
Pro_Beh_2|T:3
Pro_Beh_3|t:2
Pro_Beh_3|T:2
MEASURE|Prosocial Behavior Network
Pro_Beh_Net1|1:3
Pro_Beh_Net1|2:4
Pro_Beh_Net1|3:2
MEASURE|Advice Network
Adv_Net|1:4
Adv_Net|2:5
Adv_Net|3:1
MEASURE|Adversarial (Hindrance) Network
Hin_Net|1:2
Hin_Net|2:1
Hin_Net|3:2
MEASURE|Informal Leadership
Info_Leader_Net|1:3
Info_Leader_Net|2:5
Info_Leader_Net|3:1
MEASURE|Team Satisfaction1
TeamSat1_Net1|1:4
TeamSat1_Net1|2:5
TeamSat1_Net1|3:3
MEASURE|Information Sharing Network
IS_Net1|1:5
IS_Net1|2:3
IS_Net1|3:5
MEASURE|Transactive Memory Systems
TMS1_Net1|1:5
TMS1_Net1|2:4
TMS1_Net1|3:3
MEASURE|Transactive Memory Systems
TMS_1|T:3
TMS_2|T:3
TMS_3|T:4
TMS_4|T:3
TMS_5|T:3
TMS_6|T:4
TMS_7|T:4
TMS_8|T:4
TMS_9|T:1
TMS_10|T:0
MEASURE|Trust1_Net
Trust1_1|t:4
Trust1_1|T:4
Trust1_2|t:4
Trust1_2|T:4
Trust1_3|t:4
Trust1_3|T:4
Trust1_4|t:3
Trust1_4|T:3
Trust1_5|t:3
Trust1_5|T:3
Trust1_6|t:4
Trust1_6|T:4
MEASURE|Trust2
Trust2_Net|1:5
Trust2_Net|2:5
Trust2_Net|3:4
MEASURE|Action Process
P-Act1|2
P-Act2|2
P-Act3|4
P-Act4|3
P-Act5|3
P-Act6|4
P-Act7|4
P-Act8|3
P-Act9|4
MEASURE|Action Process
P-Act_Net|1:5
P-Act_Net|2:5
P-Act_Net|3:3
MEASURE|Transactive Memory Systems
TMS1_Net1|1:5
TMS1_Net1|2:5
TMS1_Net1|3:4
TMS2_Net2|1:5
TMS2_Net2|2:5
TMS2_Net2|3:5
TMS3_Net3|1:5
TMS3_Net3|2:5
TMS3_Net3|3:5
TMS4_Net4|1:5
TMS4_Net4|2:5
TMS4_Net4|3:4
MEASURE|Infomration Sharing
IS1|4
IS2|4
IS3|4
MEASURE|Information Sharing Network
IS_Net1|1:5
IS_Net1|2:5
IS_Net1|3:4
IS_Net2|1:5
IS_Net2|2:5
IS_Net2|3:1
MEASURE|Social Identity
SocId1|t:4
SocId1|T:4
SocId2|t:4
SocId2|T:4
SocId3|t:0
SocId3|T:0
SocId4|t:0
SocId4|T:0
SocId5|t:4
SocId5|T:4
SocId6|t:4
SocId6|T:4
SocId7|t:4
SocId7|T:4
SocId8|t:4
SocId8|T:4
SocId9|t:0
SocId9|T:0
SocId10|t:4
SocId10|T:4
MEASURE|Prosoical Behavior
Pro_Beh_1|t:0
Pro_Beh_1|T:0
Pro_Beh_2|t:4
Pro_Beh_2|T:4
Pro_Beh_3|t:4
Pro_Beh_3|T:4
MEASURE|Prosocial Behavior Network
Pro_Beh_Net1|1:4
Pro_Beh_Net1|2:4
Pro_Beh_Net1|3:1
MEASURE|Informal Leadership
Info_Leader_Net|1:4
Info_Leader_Net|2:5
Info_Leader_Net|3:3
MEASURE|Advice Network
Adv_Net|1:5
Adv_Net|2:5
Adv_Net|3:1
MEASURE|Adversarial (Hindrance) Network
Hin_Net|1:1
Hin_Net|2:1
Hin_Net|3:1
MEASURE|Team Satisfaction1
TeamSat1_Net1|1:5
TeamSat1_Net1|2:5
TeamSat1_Net1|3:4
MEASURE|Team Satisfaction2
TeamSat2_1|t:4
TeamSat2_1|T:4
TeamSat2_2|t:4
TeamSat2_2|T:4
TeamSat2_3|t:4
TeamSat2_3|T:4
MEASURE|Team Satisfaction3
TeamSat3_Net1|1:5
TeamSat3_Net1|2:5
TeamSat3_Net1|3:2
TeamSat3_Net2|1:1
TeamSat3_Net2|2:1
TeamSat3_Net2|3:2
TeamSat3_Net3|1:5
TeamSat3_Net3|2:5
TeamSat3_Net3|3:4
TeamSat3_Net4|1:4
TeamSat3_Net4|2:4
TeamSat3_Net4|3:1
MEASURE|MTML1
MTML1-1|3
MTML1-2|4
MTML1-3|4
MTML1-4|6
MTML1-5|6
MTML1-6|6
MTML1-7|6
MTML1-8|6
MTML1-9|6
MTML1-10|6
MTML1-11|6
MTML1-12|6
MTML1-13|6
MTML1-14|6
MTML1-15|6
MTML1-16|6
MTML1-17|6
MTML1-18|0
MTML1-19|0
MTML1-20|0
MTML1-21|0
MTML1-22|0
MEASURE|Friendship
Friendship1|1:3
Friendship1|2:3
Friendship1|3:1
Friendship2|1:4
Friendship2|2:3
Friendship2|3:1
