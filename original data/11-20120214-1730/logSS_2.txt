SCELL|Active;29|09:05-8:19 PM
AMODE|Move|09:08-8:19 PM
MVSLF|A3|09:09-8:19 PM
AMODE|Identify|09:19-8:19 PM
IDENT|A Barrel Threat has been identified in map cell A3 at position 247, 229.|09:33-8:20 PM
AMODE|Engage|10:27-8:21 PM
AMODE|Identify|10:30-8:21 PM
AMODE|Move|10:40-8:21 PM
AMODE|Identify|10:41-8:21 PM
SCELL|Active;39|11:28-8:22 PM
AMODE|Move|11:29-8:22 PM
MVSLF|A4|11:30-8:22 PM
AMODE|Identify|11:36-8:22 PM
IDENT|A Tire Fire Target has been identified in map cell A4 at position 238, 305.|11:50-8:22 PM
IDENT|A Van Threat has been identified in map cell A4 at position 232, 207.|12:09-8:22 PM
SCELL|Active;27|12:20-8:22 PM
AMODE|Move|12:22-8:22 PM
MVSLF|C3|12:23-8:22 PM
AMODE|Identify|12:34-8:23 PM
IDENT|A Barrel Threat has been identified in map cell C3 at position 247, 229.|12:45-8:23 PM
IDENT|A Tire Fire Target has been identified in map cell C3 at position 397, 234.|13:06-8:23 PM
IDENT|A Tire Fire Target has been identified in map cell C3 at position 110, 233.|13:18-8:23 PM
SCELL|Active;5|13:34-8:24 PM
AMODE|Move|13:35-8:24 PM
MVSLF|E1|13:36-8:24 PM
AMODE|Identify|14:32-8:25 PM
IDENT|A Tire Fire Target has been identified in map cell E1 at position 235, 82.|14:41-8:25 PM
IDENT|A Van Threat has been identified in map cell E1 at position 314, 180.|14:59-8:25 PM
SCELL|Active;35|15:12-8:25 PM
AMODE|Move|15:13-8:25 PM
MVSLF|E4|15:14-8:25 PM
AMODE|Identify|15:30-8:26 PM
IDENT|A Radio Tower Target has been identified in map cell E4 at position 406, 129.|15:41-8:26 PM
AMODE|Move|15:47-8:26 PM
SCELL|Active;84|16:09-8:26 PM
SCELL|Active;94|16:10-8:26 PM
MVSLF|F10|16:12-8:26 PM
AMODE|Identify|16:47-8:27 PM
IDENT|A Tire Fire Target has been identified in map cell F10 at position 276, 97.|16:58-8:27 PM
SCELL|Active;33|17:12-8:27 PM
SCELL|Active;83|17:17-8:27 PM
AMODE|Move|17:18-8:27 PM
MVSLF|G9|17:19-8:27 PM
AMODE|Identify|17:29-8:28 PM
IDENT|A Tire Fire Target has been identified in map cell G9 at position 71, 300.|17:38-8:28 PM
IDENT|A Tire Fire Target has been identified in map cell G9 at position 296, 429.|17:55-8:28 PM
SCELL|Active;82|18:11-8:28 PM
AMODE|Move|18:12-8:28 PM
MVSLF|H9|18:13-8:28 PM
AMODE|Identify|18:24-8:28 PM
IDENT|A Tire Fire Target has been identified in map cell H9 at position 212, 298.|18:32-8:29 PM
IDENT|A Barrel Threat has been identified in map cell H9 at position 222, 215.|18:42-8:29 PM
AMODE|Move|18:55-8:29 PM
SCELL|Active;62|18:57-8:29 PM
MVSLF|H7|19:07-8:29 PM
AMODE|Identify|19:19-8:29 PM
IDENT|A Tire Fire Target has been identified in map cell H7 at position 69, 428.|19:28-8:30 PM
IDENT|A Barrel Threat has been identified in map cell H7 at position 240, 349.|19:54-8:30 PM
AMODE|Move|20:02-8:30 PM
SCELL|Active;32|20:05-8:30 PM
MVSLF|H4|20:08-8:30 PM
AMODE|Identify|20:23-8:30 PM
IDENT|A Radio Tower Target has been identified in map cell H4 at position 74, 117.|21:24-8:40 PM
IDENT|A Radio Tower Target has been identified in map cell H4 at position 420, 106.|21:43-8:40 PM
IDENT|A Van Threat has been identified in map cell H4 at position 334, 270.|23:48-8:42 PM
SCELL|Active;23|24:07-8:42 PM
SCELL|Active;33|24:07-8:42 PM
AMODE|Move|24:09-8:42 PM
MVSLF|G4|24:09-8:42 PM
AMODE|Identify|25:41-8:44 PM
IDENT|A Tire Fire Target has been identified in map cell G4 at position 62, 325.|25:50-8:44 PM
IDENT|A Van Threat has been identified in map cell G4 at position 245, 332.|26:06-8:44 PM
AMODE|Move|26:45-8:45 PM
SCELL|Active;31|26:53-8:45 PM
MVSLF|I4|26:56-8:45 PM
AMODE|Identify|27:08-8:45 PM
IDENT|A Tire Fire Target has been identified in map cell I4 at position 203, 282.|27:24-8:46 PM
IDENT|A Radio Tower Target has been identified in map cell I4 at position 357, 94.|27:40-8:46 PM
IDENT|A Radio Tower Target has been identified in map cell I4 at position 70, 84.|28:00-8:46 PM
SCELL|Active;62|28:10-8:46 PM
AMODE|Move|28:14-8:46 PM
MVSLF|H7|28:15-8:46 PM
AMODE|Identify|28:44-8:47 PM
IDENT|A Radio Tower Target has been identified in map cell H7 at position 427, 295.|28:58-8:47 PM
SCELL|Active;80|29:11-8:47 PM
AMODE|Move|29:12-8:47 PM
MVSLF|J9|29:12-8:47 PM
AMODE|Identify|29:33-8:48 PM
IDENT|A Sedan Threat has been identified in map cell J9 at position 254, 213.|29:41-8:48 PM
IDENT|A Radio Tower Target has been identified in map cell J9 at position 55, 57.|29:55-8:48 PM
SCELL|Active;70|30:05-8:48 PM
AMODE|Move|30:06-8:48 PM
MVSLF|J8|30:07-8:48 PM
AMODE|Identify|30:12-8:48 PM
IDENT|A Radio Tower Target has been identified in map cell J8 at position 434, 94.|30:25-8:49 PM
IDENT|A Radio Tower Target has been identified in map cell J8 at position 96, 313.|30:36-8:49 PM
SCELL|Active;50|30:47-8:49 PM
SCELL|Active;40|30:48-8:49 PM
SCELL|Active;50|30:48-8:49 PM
AMODE|Move|30:49-8:49 PM
MVSLF|J6|30:50-8:49 PM
AMODE|Identify|31:01-8:49 PM
IDENT|A Tire Fire Target has been identified in map cell J6 at position 62, 220.|31:09-8:49 PM
IDENT|A Van Threat has been identified in map cell J6 at position 182, 218.|31:24-8:49 PM
IDENT|A Radio Tower Target has been identified in map cell J6 at position 367, 135.|31:35-8:50 PM
IDENT|A Radio Tower Target has been identified in map cell J6 at position 304, 54.|32:52-8:51 PM
IDENT|A Radio Tower Target has been identified in map cell J6 at position 416, 60.|33:16-8:51 PM
SCELL|Active;70|33:32-8:52 PM
AMODE|Move|33:33-8:52 PM
MVSLF|J8|33:34-8:52 PM
AMODE|Identify|34:05-8:52 PM
IDENT|A Barrel Threat has been identified in map cell J8 at position 417, 260.|34:15-8:52 PM
IDENT|A Tire Fire Target has been identified in map cell J8 at position 103, 235.|35:03-8:53 PM
SCELL|Active;80|35:11-8:53 PM
AMODE|Move|35:12-8:53 PM
MVSLF|J9|35:13-8:53 PM
AMODE|Identify|35:25-8:54 PM
IDENT|A Radio Tower Target has been identified in map cell J9 at position 235, 137.|35:36-8:54 PM
SCELL|Active;31|35:51-8:54 PM
SCELL|Active;82|35:51-8:54 PM
AMODE|Move|35:51-8:54 PM
MVSLF|H9|35:51-8:54 PM
AMODE|Identify|36:27-8:55 PM
IDENT|A Tire Fire Target has been identified in map cell H9 at position 337, 182.|36:36-8:55 PM
SCELL|Active;82|37:31-8:56 PM
AMODE|Engage|37:35-8:56 PM
AMODE|Identify|37:54-8:56 PM
SCELL|Active;31|38:10-8:56 PM
AMODE|Move|38:10-8:56 PM
MVSLF|I4|38:13-8:56 PM
AMODE|Identify|38:15-8:56 PM
IDENT|A Tire Fire Target has been identified in map cell I4 at position 347, 325.|38:52-8:57 PM
SCELL|Active;32|39:18-8:57 PM
AMODE|Move|39:18-8:57 PM
MVSLF|H4|39:18-8:57 PM
AMODE|Identify|39:58-8:58 PM
IDENT|A Tire Fire Target has been identified in map cell H4 at position 121, 284.|40:06-8:58 PM
IDENT|A Van Threat has been identified in map cell H4 at position 334, 270.|40:30-8:59 PM
SCELL|Active;23|41:12-8:59 PM
SCELL|Active;33|41:12-8:59 PM
AMODE|Move|41:12-8:59 PM
MVSLF|G4|41:15-8:59 PM
