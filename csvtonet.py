import csv
import sys

def write_net(matrix, filename):
	with open(filename, 'w') as f:
		for row in matrix:
			f.write(' '.join(row) + '\r\n')


if __name__=="__main__":
	if len(sys.argv) < 3:
		print 'Syntax: csvtonet.py inputfile.csv outputfile.csv'
		exit(1)

	input_filename = sys.argv[1]
	output_filename = sys.argv[2]
	with open(input_filename, 'r') as f:
		data = list(csv.reader(f))
		data = [d[1:] for d in data[1:]]
		write_net(data, output_filename)