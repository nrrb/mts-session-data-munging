START|3/1/2012 1:34:22 PM
MEASURE|Demographics
Gender|1
Race|4
Age|22
PCVidPlay|0
VidPlay|1
Iming|5
TeamConf|3
SkypeConf|3
VidLearnConf|3
Phase2|1
MEASURE|Competitiveness
Comp_1|2
Comp_2|1
Comp_3|2
Comp_4|2
Comp_5|3
Comp_6|0
Comp_7|0
Comp_8|1
Comp_9|0
Comp_10|1
Comp_11|0
Comp_12|2
Comp_13|2
Comp_14|0
Comp_15|0
Comp_16|2
Comp_17|0
Comp_18|3
Comp_19|2
Comp_20|0
MEASURE|Self- and Other-Orientation
Prosocial_1|1
Prosocial_2|4
Prosocial_3|2
Prosocial_4|2
Prosocial_5|2
Prosocial_6|2
MEASURE|Information Sharing Network
IS_Net1|1:3
IS_Net1|2:1
IS_Net1|3:4
MEASURE|Transactive Memory Systems
TMS1_Net1|1:4
TMS1_Net1|2:4
TMS1_Net1|3:4
MEASURE|Transactive Memory Systems
TMS_1|3
TMS_2|3
TMS_3|3
TMS_4|3
TMS_5|3
TMS_6|3
TMS_7|3
TMS_8|3
TMS_9|1
TMS_10|1
MEASURE|Trust1_Net
Trust1_1|t:4
Trust1_1|T:4
Trust1_2|t:3
Trust1_2|T:3
Trust1_3|t:3
Trust1_3|T:3
Trust1_4|t:3
Trust1_4|T:3
Trust1_5|t:3
Trust1_5|T:3
Trust1_6|t:3
Trust1_6|T:3
MEASURE|Trust2
Trust2_Net|1:4
Trust2_Net|2:4
Trust2_Net|3:4
MEASURE|Action Process
P-Act1|3
P-Act2|3
P-Act3|2
P-Act4|2
P-Act5|2
P-Act6|2
P-Act7|3
P-Act8|3
P-Act9|2
MEASURE|Action Process
P-Act_Net|1:4
P-Act_Net|2:4
P-Act_Net|3:1
MEASURE|Transactive Memory Systems
TMS1_Net|1:3
TMS1_Net|2:3
TMS1_Net|3:3
TMS2_Net|1:3
TMS2_Net|2:3
TMS2_Net|3:3
TMS3_Net|1:4
TMS3_Net|2:5
TMS3_Net|3:2
TMS4_Net|1:4
TMS4_Net|2:4
TMS4_Net|3:2
MEASURE|Infomration Sharing
IS1|4
IS2|3
IS3|3
MEASURE|Information Sharing Network
IS_Net1|1:4
IS_Net1|2:4
IS_Net1|3:2
IS_Net2|1:4
IS_Net2|2:4
IS_Net2|3:2
MEASURE|Social Identity
SocId1|t:1
SocId1|T:3
SocId2|t:2
SocId2|T:1
SocId3|t:1
SocId3|T:1
SocId4|t:1
SocId4|T:1
SocId5|t:3
SocId5|T:3
SocId6|t:3
SocId6|T:3
SocId7|t:3
SocId7|T:3
SocId8|t:3
SocId8|T:3
SocId9|t:1
SocId9|T:1
SocId10|t:1
SocId10|T:1
MEASURE|Prosoical Behavior
Pro_Beh_1|t:1
Pro_Beh_1|T:1
Pro_Beh_2|t:3
Pro_Beh_2|T:3
Pro_Beh_3|t:3
Pro_Beh_3|T:3
MEASURE|Prosocial Behavior Network
Pro_Beh_Net1|1:4
Pro_Beh_Net1|2:4
Pro_Beh_Net1|3:4
MEASURE|Advice Network
Adv_Net|1:5
Adv_Net|2:5
Adv_Net|3:1
MEASURE|Adversarial (Hindrance) Network
Hin_Net|1:1
Hin_Net|2:1
Hin_Net|3:1
MEASURE|Informal Leadership
Info_Leader_Net|1:1
Info_Leader_Net|2:2
Info_Leader_Net|3:1
MEASURE|Team Satisfaction1
TeamSat1_Net1|1:4
TeamSat1_Net1|2:4
TeamSat1_Net1|3:4
MEASURE|Information Sharing Network
IS_Net1|1:4
IS_Net1|2:4
IS_Net1|3:4
MEASURE|Transactive Memory Systems
TMS1_Net1|1:4
TMS1_Net1|2:4
TMS1_Net1|3:4
MEASURE|Transactive Memory Systems
TMS_1|T:3
TMS_2|T:3
TMS_3|T:3
TMS_4|T:3
TMS_5|T:3
TMS_6|T:3
TMS_7|T:3
TMS_8|T:3
TMS_9|T:1
TMS_10|T:0
MEASURE|Trust1_Net
Trust1_1|t:3
Trust1_1|T:3
Trust1_2|t:3
Trust1_2|T:3
Trust1_3|t:3
Trust1_3|T:3
Trust1_4|t:3
Trust1_4|T:3
Trust1_5|t:3
Trust1_5|T:3
Trust1_6|t:3
Trust1_6|T:3
MEASURE|Trust2
Trust2_Net|1:5
Trust2_Net|2:5
Trust2_Net|3:5
MEASURE|Action Process
P-Act1|3
P-Act2|3
P-Act3|3
P-Act4|3
P-Act5|3
P-Act6|3
P-Act7|3
P-Act8|3
P-Act9|3
MEASURE|Action Process
P-Act_Net|1:4
P-Act_Net|2:4
P-Act_Net|3:1
MEASURE|Transactive Memory Systems
TMS1_Net1|1:4
TMS1_Net1|2:4
TMS1_Net1|3:4
TMS2_Net2|1:4
TMS2_Net2|2:5
TMS2_Net2|3:4
TMS3_Net3|1:4
TMS3_Net3|2:4
TMS3_Net3|3:4
TMS4_Net4|1:4
TMS4_Net4|2:4
TMS4_Net4|3:4
MEASURE|Infomration Sharing
IS1|3
IS2|4
IS3|4
MEASURE|Information Sharing Network
IS_Net1|1:4
IS_Net1|2:5
IS_Net1|3:4
IS_Net2|1:4
IS_Net2|2:5
IS_Net2|3:4
MEASURE|Social Identity
SocId1|t:1
SocId1|T:1
SocId2|t:4
SocId2|T:4
SocId3|t:1
SocId3|T:1
SocId4|t:1
SocId4|T:1
SocId5|t:3
SocId5|T:3
SocId6|t:3
SocId6|T:3
SocId7|t:3
SocId7|T:3
SocId8|t:3
SocId8|T:3
SocId9|t:0
SocId9|T:0
SocId10|t:3
SocId10|T:3
MEASURE|Prosoical Behavior
Pro_Beh_1|t:3
Pro_Beh_1|T:3
Pro_Beh_2|t:3
Pro_Beh_2|T:3
Pro_Beh_3|t:3
Pro_Beh_3|T:3
MEASURE|Prosocial Behavior Network
Pro_Beh_Net1|1:4
Pro_Beh_Net1|2:4
Pro_Beh_Net1|3:4
MEASURE|Informal Leadership
Info_Leader_Net|1:2
Info_Leader_Net|2:4
Info_Leader_Net|3:2
MEASURE|Advice Network
Adv_Net|1:5
Adv_Net|2:5
Adv_Net|3:2
MEASURE|Adversarial (Hindrance) Network
Hin_Net|1:1
Hin_Net|2:1
Hin_Net|3:1
MEASURE|Team Satisfaction1
TeamSat1_Net1|1:5
TeamSat1_Net1|2:5
TeamSat1_Net1|3:5
MEASURE|Team Satisfaction2
TeamSat2_1|t:4
TeamSat2_1|T:4
TeamSat2_2|t:4
TeamSat2_2|T:4
TeamSat2_3|t:4
TeamSat2_3|T:4
MEASURE|Team Satisfaction3
TeamSat3_Net1|1:4
TeamSat3_Net1|2:5
TeamSat3_Net1|3:4
TeamSat3_Net2|1:1
TeamSat3_Net2|2:1
TeamSat3_Net2|3:1
TeamSat3_Net3|1:3
TeamSat3_Net3|2:4
TeamSat3_Net3|3:3
TeamSat3_Net4|1:4
TeamSat3_Net4|2:4
TeamSat3_Net4|3:3
MEASURE|MTML1
MTML1-1|5
MTML1-2|2
MTML1-3|2
MTML1-4|5
MTML1-5|2
MTML1-6|5
MTML1-7|5
MTML1-8|5
MTML1-9|6
MTML1-10|2
MTML1-11|5
MTML1-12|6
MTML1-13|2
MTML1-14|1
MTML1-15|5
MTML1-16|1
MTML1-17|1
MTML1-18|4
MTML1-19|1
MTML1-20|0
MTML1-21|0
MTML1-22|0
MEASURE|Friendship
Friendship1|1:1
Friendship1|2:3
Friendship1|3:1
Friendship2|1:1
Friendship2|2:3
Friendship2|3:1
