START|2/29/2012 1:44:52 PM
MEASURE|Demographics
Gender|0
Race|3
Age|34
PCVidPlay|0
VidPlay|2
Iming|5
TeamConf|2
SkypeConf|0
VidLearnConf|1
Phase2|1
MEASURE|Competitiveness
Comp_1|2
Comp_2|0
Comp_3|0
Comp_4|3
Comp_5|2
Comp_6|2
Comp_7|2
Comp_8|3
Comp_9|0
Comp_10|2
Comp_11|2
Comp_12|3
Comp_13|3
Comp_14|3
Comp_15|3
Comp_16|3
Comp_17|3
Comp_18|3
Comp_19|3
Comp_20|0
MEASURE|Self- and Other-Orientation
Prosocial_1|2
Prosocial_2|2
Prosocial_3|3
Prosocial_4|2
Prosocial_5|3
Prosocial_6|3
MEASURE|Information Sharing Network
IS_Net1|1:4
IS_Net1|2:4
IS_Net1|3:4
MEASURE|Transactive Memory Systems
TMS1_Net1|1:3
TMS1_Net1|2:3
TMS1_Net1|3:3
MEASURE|Transactive Memory Systems
TMS_1|3
TMS_2|3
TMS_3|3
TMS_4|4
TMS_5|2
TMS_6|3
TMS_7|3
TMS_8|3
TMS_9|3
TMS_10|1
MEASURE|Trust1_Net
Trust1_1|t:3
Trust1_1|T:3
Trust1_2|t:3
Trust1_2|T:3
Trust1_3|t:4
Trust1_3|T:3
Trust1_4|t:3
Trust1_4|T:3
Trust1_5|t:3
Trust1_5|T:3
Trust1_6|t:3
Trust1_6|T:3
MEASURE|Trust2
Trust2_Net|1:4
Trust2_Net|2:4
Trust2_Net|3:4
MEASURE|Action Process
P-Act1|3
P-Act2|3
P-Act3|3
P-Act4|3
P-Act5|3
P-Act6|3
P-Act7|2
P-Act8|3
P-Act9|4
MEASURE|Action Process
P-Act_Net|1:4
P-Act_Net|2:4
P-Act_Net|3:4
MEASURE|Transactive Memory Systems
TMS1_Net|1:3
TMS1_Net|2:3
TMS1_Net|3:3
TMS2_Net|1:4
TMS2_Net|2:4
TMS2_Net|3:4
TMS3_Net|1:3
TMS3_Net|2:3
TMS3_Net|3:4
TMS4_Net|1:4
TMS4_Net|2:4
TMS4_Net|3:4
MEASURE|Infomration Sharing
IS1|3
IS2|3
IS3|3
MEASURE|Information Sharing Network
IS_Net1|1:4
IS_Net1|2:4
IS_Net1|3:3
IS_Net2|1:3
IS_Net2|2:3
IS_Net2|3:3
MEASURE|Social Identity
SocId1|t:2
SocId1|T:2
SocId2|t:3
SocId2|T:3
SocId3|t:3
SocId3|T:3
SocId4|t:3
SocId4|T:3
SocId5|t:3
SocId5|T:3
SocId6|t:3
SocId6|T:3
SocId7|t:3
SocId7|T:3
SocId8|t:3
SocId8|T:3
SocId9|t:3
SocId9|T:3
SocId10|t:3
SocId10|T:3
MEASURE|Prosoical Behavior
Pro_Beh_1|t:3
Pro_Beh_1|T:3
Pro_Beh_2|t:3
Pro_Beh_2|T:3
Pro_Beh_3|t:3
Pro_Beh_3|T:3
MEASURE|Prosocial Behavior Network
Pro_Beh_Net1|1:4
Pro_Beh_Net1|2:4
Pro_Beh_Net1|3:4
MEASURE|Advice Network
Adv_Net|1:3
Adv_Net|2:3
Adv_Net|3:3
MEASURE|Adversarial (Hindrance) Network
Hin_Net|1:3
Hin_Net|2:3
Hin_Net|3:3
MEASURE|Informal Leadership
Info_Leader_Net|1:3
Info_Leader_Net|2:3
Info_Leader_Net|3:3
MEASURE|Team Satisfaction1
TeamSat1_Net1|1:4
TeamSat1_Net1|2:4
TeamSat1_Net1|3:4
MEASURE|Information Sharing Network
IS_Net1|1:3
IS_Net1|2:3
IS_Net1|3:3
MEASURE|Transactive Memory Systems
TMS1_Net1|1:3
TMS1_Net1|2:3
TMS1_Net1|3:3
MEASURE|Transactive Memory Systems
TMS_1|T:3
TMS_2|T:3
TMS_3|T:3
TMS_4|T:3
TMS_5|T:3
TMS_6|T:3
TMS_7|T:3
TMS_8|T:3
TMS_9|T:3
TMS_10|T:3
MEASURE|Trust1_Net
Trust1_1|t:3
Trust1_1|T:3
Trust1_2|t:3
Trust1_2|T:3
Trust1_3|t:3
Trust1_3|T:3
Trust1_4|t:3
Trust1_4|T:3
Trust1_5|t:3
Trust1_5|T:3
Trust1_6|t:4
Trust1_6|T:4
MEASURE|Trust2
Trust2_Net|1:5
Trust2_Net|2:4
Trust2_Net|3:4
MEASURE|Action Process
P-Act1|3
P-Act2|3
P-Act3|3
P-Act4|3
P-Act5|3
P-Act6|3
P-Act7|3
P-Act8|3
P-Act9|3
MEASURE|Action Process
P-Act_Net|1:4
P-Act_Net|2:4
P-Act_Net|3:4
MEASURE|Transactive Memory Systems
TMS1_Net1|1:3
TMS1_Net1|2:3
TMS1_Net1|3:3
TMS2_Net2|1:4
TMS2_Net2|2:4
TMS2_Net2|3:4
TMS3_Net3|1:4
TMS3_Net3|2:4
TMS3_Net3|3:4
TMS4_Net4|1:4
TMS4_Net4|2:4
TMS4_Net4|3:4
MEASURE|Infomration Sharing
IS1|3
IS2|3
IS3|3
MEASURE|Information Sharing Network
IS_Net1|1:4
IS_Net1|2:4
IS_Net1|3:4
IS_Net2|1:4
IS_Net2|2:4
IS_Net2|3:4
MEASURE|Social Identity
SocId1|t:2
SocId1|T:2
SocId2|t:3
SocId2|T:3
SocId3|t:2
SocId3|T:2
SocId4|t:3
SocId4|T:3
SocId5|t:3
SocId5|T:3
SocId6|t:3
SocId6|T:3
SocId7|t:3
SocId7|T:3
SocId8|t:3
SocId8|T:3
SocId9|t:2
SocId9|T:2
SocId10|t:3
SocId10|T:3
MEASURE|Prosoical Behavior
Pro_Beh_1|t:3
Pro_Beh_1|T:3
Pro_Beh_2|t:3
Pro_Beh_2|T:3
Pro_Beh_3|t:3
Pro_Beh_3|T:3
MEASURE|Prosocial Behavior Network
Pro_Beh_Net1|1:4
Pro_Beh_Net1|2:4
Pro_Beh_Net1|3:4
MEASURE|Informal Leadership
Info_Leader_Net|1:4
Info_Leader_Net|2:4
Info_Leader_Net|3:4
MEASURE|Advice Network
Adv_Net|1:4
Adv_Net|2:4
Adv_Net|3:4
MEASURE|Adversarial (Hindrance) Network
Hin_Net|1:4
Hin_Net|2:4
Hin_Net|3:4
MEASURE|Team Satisfaction1
TeamSat1_Net1|1:4
TeamSat1_Net1|2:4
TeamSat1_Net1|3:4
MEASURE|Team Satisfaction2
TeamSat2_1|t:3
TeamSat2_1|T:3
TeamSat2_2|t:3
TeamSat2_2|T:3
TeamSat2_3|t:3
TeamSat2_3|T:3
MEASURE|Team Satisfaction3
TeamSat3_Net1|1:4
TeamSat3_Net1|2:4
TeamSat3_Net1|3:4
TeamSat3_Net2|1:4
TeamSat3_Net2|2:4
TeamSat3_Net2|3:4
TeamSat3_Net3|1:4
TeamSat3_Net3|2:4
TeamSat3_Net3|3:4
TeamSat3_Net4|1:4
TeamSat3_Net4|2:4
TeamSat3_Net4|3:4
MEASURE|MTML1
MTML1-1|5
MTML1-2|4
MTML1-3|5
MTML1-4|5
MTML1-5|5
MTML1-6|5
MTML1-7|5
MTML1-8|5
MTML1-9|5
MTML1-10|5
MTML1-11|5
MTML1-12|0
MTML1-13|5
MTML1-14|3
MTML1-15|4
MTML1-16|4
MTML1-17|4
MTML1-18|4
MTML1-19|4
MTML1-20|5
MTML1-21|5
MTML1-22|5
MEASURE|Friendship
Friendship1|1:3
Friendship1|2:3
Friendship1|3:3
Friendship2|1:3
Friendship2|2:3
Friendship2|3:3
