import csv
import os

# These substitutions were determined manually by Toshio, 4/17/2012
fn_translation = \
{('Action Process','3','P-Act_Net'): 'Action Process phase3',
('Action Process','5','P-Act_Net'): 'Action Process phase5',
('Adversarial (Hindrance) Network','3','Hin_Net'): 'Adversarial phase3',
('Adversarial (Hindrance) Network','6','Hin_Net'): 'Adversarial phase6',
('Advice Network','3','Adv_Net'): 'Advice phase3',
('Advice Network','6','Adv_Net'): 'Advice phase6',
('Friendship','6','Friendship1'): 'Friendship1',
('Friendship','6','Friendship2'): 'Friendship2',
('Informal Leadership','3','Info_Leader_Net'): 'Informal Leadership phase3',
('Informal Leadership','5','Info_Leader_Net'): 'Informal Leadership phase5',
('Information Sharing Network','2','IS_Net1'): 'Information Sharing1 phase2',
('Information Sharing Network','3','IS_Net1'): 'Information Sharing1 phase3',
('Information Sharing Network','3','IS_Net2'): 'Information Sharing2 phase3',
('Information Sharing Network','4','IS_Net1'): 'Information Sharing1 phase4',
('Information Sharing Network','4','IS_Net2'): 'Information Sharing2 phase4',
('Information Sharing Network','5','IS_Net1'): 'Information Sharing1 phase5',
('Information Sharing Network','5','IS_Net2'): 'Information Sharing2 phase5',
('Prosocial Behavior Network','3','Pro_Beh_Net1'): 'Prosocial Behavior phase3',
('Prosocial Behavior Network','5','Pro_Beh_Net1'): 'Prosocial Behavior phase5',
('Team Satisfaction1','3','TeamSat1_Net1'): 'Team Satisfaction1 phase3',
('Team Satisfaction1','6','TeamSat1_Net1'): 'Team Satisfaction1 phase6',
('Team Satisfaction3','6','TeamSat3_Net1'): 'Team Liking phase6',
('Team Satisfaction3','6','TeamSat3_Net2'): 'Team Frustration phase6',
('Team Satisfaction3','6','TeamSat3_Net3'): 'Team Helping phase6',
('Team Satisfaction3','6','TeamSat3_Net4'): 'Team Supporting phase6',
('Transactive Memory Systems','2','TMS1_Net1'): 'TMS1 phase2',
('Transactive Memory Systems','3','TMS1_Net'): 'TMS1 phase3',
('Transactive Memory Systems','3','TMS2_Net'): 'TMS2 phase3',
('Transactive Memory Systems','3','TMS3_Net'): 'TMS3 phase3',
('Transactive Memory Systems','3','TMS4_Net'): 'TMS4 phase3',
('Transactive Memory Systems','4','TMS1_Net1'): 'TMS1 phase4',
('Transactive Memory Systems','4','TMS2_Net2'): 'TMS2 phase4',
('Transactive Memory Systems','4','TMS3_Net3'): 'TMS3 phase4',
('Transactive Memory Systems','4','TMS4_Net4'): 'TMS4 phase4',
('Transactive Memory Systems','5','TMS1_Net1'): 'TMS1 phase5',
('Transactive Memory Systems','5','TMS2_Net2'): 'TMS2 phase5',
('Transactive Memory Systems','5','TMS3_Net3'): 'TMS3 phase5',
('Transactive Memory Systems','5','TMS4_Net4'): 'TMS4 phase5',
('Trust2','3','Trust2_Net'): 'Trust phase3',
('Trust2','5','Trust2_Net'): 'Trust phase5'}

network_targets = \
{('Phantom Recon', '1'): 'Stinger Recon',
('Phantom Recon', '2'): 'Stinger Specialist',
('Phantom Recon', '3'): 'Phantom Specialist',
('Phantom Specialist', '1'): 'Stinger Recon',
('Phantom Specialist', '2'): 'Stinger Specialist',
('Phantom Specialist', '3'): 'Phantom Recon',
('Stinger Recon', '1'): 'Stinger Specialist',
('Stinger Recon', '2'): 'Phantom Recon',
('Stinger Recon', '3'): 'Phantom Specialist',
('Stinger Specialist', '1'): 'Stinger Recon',
('Stinger Specialist', '2'): 'Phantom Recon',
('Stinger Specialist', '3'): 'Phantom Specialist'}

def make_csv_adjacency_matrix(triples, headers=True):
	''' 
	Given a list of triples, (person1, person2, value), create an 
	adjacency matrix in CSV form. Will take union of all person1
	and person2 values to form population in matrix, and fill in
	zeroes for values not provided. 
	'''
	people = list(set([p1 for p1,p2,v in triples] + [p2 for p1,p2,v in triples]))
	people = sorted(people)
	# Create a dict of dicts to aid creating the matrix
	matrix = dict()
	for p1, p2, v in triples:
		matrix[p1] = matrix.get(p1, dict())
		matrix[p1][p2] = v
	# Fill in zeroes for anyone not included
	for p1 in people:
		matrix[p1] = matrix.get(p1, dict())
		for p2 in people:
			matrix[p1][p2] = matrix[p1].get(p2, 0)
	# Now form the rows of the matrix using the ordered list of people
	csv_matrix = []
	csv_matrix.append([''] + people)
	for p1 in people:
		row = [p1]
		for p2 in people:
			row.append(matrix[p1][p2])
		csv_matrix.append(row)
	return csv_matrix


with open('./original_data/alldata.csv', 'r') as f:
    dr = csv.DictReader(f)
    data = list(dr)

for value_func, output_dir in [(lambda x: x, './processed_data/adjacency_matrices/original_values/'), 
		(lambda x: int(int(x) > 3), './processed_data/adjacency_matrices/dichotomized_4_plus/'),
		(lambda x: int(int(x) > 2), './processed_data/adjacency_matrices/dichotomized_3_plus/')]:
	adjacency_matrices = dict()
	for datum in data:
		if datum['Measure Type'] == 'Q':
			continue
		fn_temp = fn_translation[(datum['Measure Name'],datum['Phase'],datum['Variable Name'])]
		fn_temp = fn_temp.replace(' ', '_') +'.csv'
		filename = os.path.join(output_dir, fn_temp)
		network_target_num, response = datum['Response'].split(':')
		network_target = network_targets[(datum['Role'], network_target_num)]
		response = value_func(response)
		adjacency_matrices[filename] = adjacency_matrices.get(filename, list())
		adjacency_matrices[filename].append([datum['Role'], network_target, response])

	for filename, adjacency_matrix in adjacency_matrices.iteritems():
		print 'Creating adjacency matrix for %s.' % (filename)
		with open(filename, 'w') as f:
			writer = csv.writer(f)
			writer.writerows(make_csv_adjacency_matrix(adjacency_matrix))
