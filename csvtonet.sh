#!/bin/bash
for filename in $(ls ./processed_data/adjacency_matrices/original_values/*.csv); do
	echo "Processing $filename..."
	python csvtonet.py "${filename}" "$(dirname $filename)/$(basename $filename .csv).net"
done
for filename in $(ls ./processed_data/adjacency_matrices/dichotomized_3_plus/*.csv); do
	echo "Processing $filename..."
	python csvtonet.py "${filename}" "$(dirname $filename)/$(basename $filename .csv).net"
done
for filename in $(ls ./processed_data/adjacency_matrices/dichotomized_4_plus/*.csv); do
	echo "Processing $filename..."
	python csvtonet.py "${filename}" "$(dirname $filename)/$(basename $filename .csv).net"
done