import scipy
import csv
import os

data_path = './original data'
sessions = \
	('11-20120214-1730',
	 '12-20120215-1703',
	 '13-20120216-1722',
	 '14-20120221-1301',
	 '15-20120222-1340',
	 '16-20120222-1724',
	 '17-20120223-1321',
	 '18-20120223-1733',
	 '19-20120228-1324',
	 '20-20120229-1339',
	 '21-20120301-1330',
	 '22-20120301-1800',
	 '23-20120302-1335',
	 '24-20120402-1736',
	 '25-20120403-1800',
	 '26-20120406-1400',
	 '27-20120406-1707',
	 '28-20120410-1740')
filenames = {'answersPR.txt':'Phantom Recon', 
				'answersPS.txt':'Phantom Specialist', 
				'answersSR.txt':'Stinger Recon', 
				'answersSS.txt':'Stinger Specialist'}
measure_translation = \
{('Demographics', 0):(1, 'Demographics', 'Q'),
	('Competitiveness', 0):(1, 'Competitiveness', 'Q'),
	('Self- and Other-Orientation', 0):(1, 'Self- and Other-Orientation', 'Q'),
	('Information Sharing Network', 0):(2, 'Information Sharing Network', 'N'),
	('Transactive Memory Systems', 0):(2, 'Transactive Memory Systems', 'N'),
	('Transactive Memory Systems', 1):(2, 'Transactive Memory Systems', 'Q'),
	('Trust1_Net', 0):(3, 'Trust1_Net', 'Q'),
	('Trust2', 0):(3, 'Trust2', 'N'),
	('Action Process', 0):(3, 'Action Process', 'Q'),
	('Action Process', 1):(3, 'Action Process', 'N'),
	('Transactive Memory Systems', 2):(3, 'Transactive Memory Systems', 'N'),
	('Infomration Sharing', 0):(3, 'Infomration Sharing', 'Q'),
	('Information Sharing Network', 1):(3, 'Information Sharing Network', 'N'),
	('Social Identity', 0):(3, 'Social Identity', 'Q'),
	('Prosoical Behavior', 0):(3, 'Prosoical Behavior', 'Q'),
	('Prosocial Behavior Network', 0):(3, 'Prosocial Behavior Network', 'N'),
	('Advice Network', 0):(3, 'Advice Network', 'N'),
	('Adversarial (Hindrance) Network', 0):(3, 'Adversarial (Hindrance) Network', 'N'),
	('Informal Leadership', 0):(3, 'Informal Leadership', 'N'),
	('Team Satisfaction1', 0):(3, 'Team Satisfaction1', 'N'),
	('Information Sharing Network', 2):(4, 'Information Sharing Network', 'N'),
	('Transactive Memory Systems', 3):(4, 'Transactive Memory Systems', 'N'),
	('Transactive Memory Systems', 4):(4, 'Transactive Memory Systems', 'Q'),
	('Trust1_Net', 1):(5, 'Trust1_Net', 'Q'),
	('Trust2', 1):(5, 'Trust2', 'N'),
	('Action Process', 2):(5, 'Action Process', 'Q'),
	('Action Process', 3):(5, 'Action Process', 'N'),
	('Transactive Memory Systems', 5):(5, 'Transactive Memory Systems', 'N'),
	('Infomration Sharing', 1):(5, 'Infomration Sharing', 'Q'),
	('Information Sharing Network', 3):(5, 'Information Sharing Network', 'N'),
	('Social Identity', 1):(5, 'Social Identity', 'Q'),
	('Prosoical Behavior', 1):(5, 'Prosoical Behavior', 'Q'),
	('Prosocial Behavior Network', 1):(5, 'Prosocial Behavior Network', 'N'),
	('Informal Leadership', 1):(5, 'Informal Leadership', 'N'),
	('Advice Network', 1):(6, 'Advice Network', 'N'),
	('Adversarial (Hindrance) Network', 1):(6, 'Adversarial (Hindrance) Network', 'N'),
	('Team Satisfaction1', 1):(6, 'Team Satisfaction1', 'N'),
	('Team Satisfaction2', 0):(6, 'Team Satisfaction2', 'Q'),
	('Team Satisfaction3', 0):(6, 'Team Satisfaction3', 'N'),
	('MTML1', 0):(6, 'MTML1', 'Q'),
	('Friendship', 0):(6, 'Friendship', 'N')}
field_order = ['MTS Session','Role','Phase', 'Measure Name','Measure Type','Variable Name','Response']

allvalues = []
for session in sessions:
	for filename in filenames:
		filepath = os.path.join(data_path, session, filename)
		print 'Processing %s...' % filepath
		with open(filepath, 'r') as f:
			data = scipy.array([line.strip().split('|') for line in f.readlines()])

		measures = []
		for n, datum in enumerate(data):
			if datum[0]=='MEASURE':
				measures.append({'MTS Session':session,'Role':filenames[filename],
								'lineno': n, 'Measure Name': datum[1]})
		justmeasures = [m['Measure Name'] for m in measures]

		for n, measure in enumerate(measures):
			measure_idx = justmeasures[:n].count(measure['Measure Name'])
			if (measure['Measure Name'], measure_idx) not in measure_translation:
				# We don't want these, skip the loop
				continue
			measure['Phase'], mname, measure['Measure Type'] = measure_translation[(measure['Measure Name'], measure_idx)]
			data_start = measure['lineno'] + 1
			if n+1 >= len(measures):
				data_end = len(data)
			else:
				# The line number of the next measure
				data_end = measures[n + 1]['lineno']
			for var, response in data[data_start:data_end]:
				if var == 'START':
					# Not data we're interested in
					continue
				datum = measure.copy()
				# Special cases of questions where the response is given with respect to 't' or 'T'
				if measure['Measure Type']=='Q' and ':' in response:
					t, r = response.split(':')
					datum['Variable Name'] = '%s_%s' % (var, t)
					datum['Response'] = r
				else:
					datum['Variable Name'] = var
					datum['Response'] = response
				# We don't need to hang on to this line number
				datum.pop('lineno')
				allvalues.append(datum)

with open(os.path.join(data_path, 'alldata.csv'), 'w') as f:
	dw = csv.DictWriter(f, fieldnames=field_order)
	dw.writeheader()
	dw.writerows(allvalues)