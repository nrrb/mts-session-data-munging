import csv

role_abbrev = \
{'Phantom Recon': 'PR',
'Phantom Specialist': 'PS',
'Stinger Recon': 'SR',
'Stinger Specialist': 'SS'}

def make_csv_question_matrix(responses):
	'''
	We want to make a chart that shows the response of a given Role
	from a given MTS Session to a given Variable ID in the following
	form:

	session,role,PR_varid1,PR_varid2,PR_varid3,...,PS_varid1,...
	11,response1,response2,response3
	11,response1,response2,response3
	'''
	variables = ['%s_%s'%(role_abbrev[role], varid) for s, role, varid, r in responses]
	variables = sorted(set(variables))

	matrix = dict()
	# Pre-populate with invalid values to compensate
	# for fields that aren't populated. This addresses specifically
	# the lack of answers for Stinger Specialist in session 17 in 
	# phase 4.
	for session, _1, _2, _3 in responses:
		matrix[session] = dict()
		for varid in variables:
			matrix[session][varid] = '-1'

	for session, role, varid, response in responses:
		varid = '%s_%s'%(role_abbrev[role], varid)
		matrix[session][varid] = response
	csv_matrix = []
	row = ['MTS Session'] + variables
	csv_matrix.append(row)
	for session in sorted(matrix.keys()):
		row = [session]
		for varid in variables:
			row.append(matrix[session][varid])
		csv_matrix.append(row)
	return csv_matrix


with open('./original_data/alldata.csv', 'r') as f:
    dr = csv.DictReader(f)
    data = list(dr)

for value_func, output_dir in [(lambda x: x, './processed_data/question_matrices/original_values/'), 
		(lambda x: int(int(x) > 3), './processed_data/question_matrices/dichotomized_4_plus/'),
		(lambda x: int(int(x) > 2), './processed_data/question_matrices/dichotomized_3_plus/')]:
	question_matrices = dict()
	for datum in data:
		if datum['Measure Type'] == 'N':
			continue
		filename = output_dir + ('%s_%s.csv' % (datum['Measure Name'], datum['Phase'])).replace(' ','_')
		question_matrices[filename] = question_matrices.get(filename, list())
		question_matrices[filename].append([datum['MTS Session'], 
											datum['Role'], 
											datum['Variable Name'], 
											value_func(datum['Response'])])
	for filename, question_matrix in question_matrices.iteritems():
		print 'Creating question matrix for %s.' % filename
		with open(filename, 'w') as f:
			writer = csv.writer(f)
			writer.writerows(make_csv_question_matrix(question_matrix))
